<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestOrganizerContestListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('contest_organizer_contest_lists');
        Schema::create('contest_organizer_contest_lists', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('contest_name',200);
            $table->string('start_date',60);
            $table->string('end_date',60);
            $table->string('detail_page_url',200);
            $table->integer('contest_organizer_id')->unsigned();
            $table->timestamps();
            $table->foreign('contest_organizer_id')->references('id')->on('contest_organizers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contest_organizer_contest_lists');
//        Schema::dropIfExists('contest_organizer_contest_lists');
//        Schema::table('contest_organizer_contest_lists', function (Blueprint $table) {
//            //
//        });
    }
}
