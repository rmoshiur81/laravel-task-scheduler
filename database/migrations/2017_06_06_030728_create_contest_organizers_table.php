<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestOrganizersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('contest_organizers');
        Schema::create('contest_organizers', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('organizer_name',180);
            $table->string('organizer_website_url',120);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('contest_organizers');
        Schema::dropIfExists('contest_organizers');
//        Schema::table('contest_organizers', function (Blueprint $table) {
//            //
//        });
    }
}
