<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon ;
class ContestOrganizersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Model::unguard();
        $contest_organizers[] = ['organizer_name' => 'Hacker Rank','organizer_website_url' => 'https://hackerrank.com','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')];
        $contest_organizers[] = ['organizer_name' => 'Toph','organizer_website_url' => 'https://toph.co','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')];
//        DB::table('contest_organizers')->insert([]);
        foreach ($contest_organizers as $contest_organizer){
            $db = DB::table('contest_organizers')->insert($contest_organizer);
        }

    }
}
