<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::get('/','TophContestController@saveTophData');
Route::get('/hackerrank','ContestController@hackerRankContestList');
//Route::get('/toph','ContestController@retriveTophContestList');
//Route::get('/tophFinal','ContestController@finallyGrabContestsFromToph');
Route::get('/save-hacker-data','HackerRankController@saveHackerRData');
Route::get('/save-toph-data','TophContestController@saveTophData');
