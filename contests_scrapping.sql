drop database if exists contests_scrapping;
create database contests_scrapping;
use contests_scrapping;

create table contest_organizer_contest_lists
(
	id int(10) not null auto_increment
		primary key,
	contest_name varchar(200) not null,
	start_date varchar(60) null,
	end_date varchar(60) null,
	detail_page_url varchar(200) not null,
	contest_organizer_id int(3) not null,
	created_at timestamp null,
	updated_at timestamp null
)
;

create index contest_organizer_id
	on contest_organizer_contest_lists (contest_organizer_id)
;

create table contest_organizers
(
	id int(3) not null auto_increment
		primary key,
	organizer_name varchar(180) null,
	organizer_website_url varchar(120) null,
	created_at timestamp null,
	updated_at timestamp null
);

create table migrations
(
	id int(10) unsigned not null auto_increment
		primary key,
	migration varchar(191) not null,
	batch int not null
)
;

create table password_resets
(
	email varchar(191) not null,
	token varchar(191) not null,
	created_at timestamp null
)
;

create index password_resets_email_index
	on password_resets (email)
;

create table toph_contests
(
	id int(10) unsigned not null auto_increment
		primary key,
	contest_name varchar(200) not null,
	contest_detail_page_link varchar(200) not null,
	contest_date_time_status varchar(200) not null,
	created_at timestamp null,
	updated_at timestamp null
)
;

create table users
(
	id int(10) unsigned not null auto_increment
		primary key,
	name varchar(191) not null,
	email varchar(191) not null,
	password varchar(191) not null,
	remember_token varchar(100) null,
	created_at timestamp null,
	updated_at timestamp null,
	constraint users_email_unique
		unique (email)
)
;

