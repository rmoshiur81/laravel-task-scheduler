<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestOrganizerContestList extends Model
{
    public function contestOrganizers(){
        return $this->belongsTo('App\ContestOrganizer');
    }
}
