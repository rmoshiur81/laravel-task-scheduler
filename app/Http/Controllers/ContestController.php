<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

//use GuzzleHttp\Client as G

class ContestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $guzzleClient;

    protected $tophSelectors = [
        'contestNames' => [
            'filter' => ".table-light tr td:nth-last-child(2) a",
            'extract' => '_text',
        ],
        'links' => [
            'filter' => '.table-light tr td:nth-last-child(2) a',
            'extract' => 'href',
        ],
        'start_date_time_status' => [
            'filter' => '.table-light tr td:nth-last-child(1)',
            'extract' => '_text',
        ],
    ];

    protected $tophPaginationSelector = [
        'paginations' => [
            'filter' => '.pagination li.page-active + li a',
            'extract' => 'href',
        ],
    ];
    protected $hackerRankSelectors = [
        'contestNameSelector' => '.contest-tab-expander .contest-name',
        'contestStatusSelector' => '.contest-tab-expander .contest-status',
        'contestLinkActiveSelector' => '.contest-tab-expander a.details-link',
        'contestLinkArchiveSelector' => '.contest-tab-expander span.details-link',
    ];

    function __construct()
    {
        date_default_timezone_set('Asia/Dhaka');
        $this->guzzleClient = new Client();
        $this->tophSelectors;
        $this->tophPaginationSelector;
        $this->hackerRankSelectors;
    }

    //    start of toph contest list grabing task

    public function retriveDataList($url, $selector, $toBeExtract)
    {
        $crawler = $this->guzzleClient->request('GET', $url);
        $output = $crawler->filter($selector)->extract($toBeExtract);
        return $output;
    }


    protected function grabData($selectors, $url)
    {
        $output = [];
        foreach ($selectors as $key => $selector) {
            $selectorF = $selector['filter'];
            $toBeExtract = $selector['extract'];
            $output[$key] = Self::retriveDataList($url, $selectorF, $toBeExtract);
        }
        return $output;
    }

    protected function buildUrl($url, $uri)
    {
        return $url . $uri;
    }

    public function finallyGrabContestsFromToph()
    {
        $data = self::retriveTophContestList();
        $totalIndex = count($data);
        $arrangedData = $data[0];
        if (count($data) > 0) {
            for ($i = 1; $i < $totalIndex; $i++) {
                $arrangedData = array_merge_recursive($data[0], $data[$i]);
            }
        }

//        echo "<pre>";
//        print_r($arrangedData);
        return ($arrangedData);
    }

    public function retriveTophContestList($startPage = 0)
    {
        $output = [];
        $url = "https://toph.co/contests?start=";
        $url = self::buildUrl($url, $startPage);
        $arr[] = self::retriveTophPerContestPage($url);
        $paginationOutput = self::retriveTophPerPagination($url);
        if (count($paginationOutput['paginations']) == 1) {
            $startPage = preg_replace("/[^0-9]/", "", $paginationOutput['paginations'][0]);
            $ar = self::retriveTophContestList($startPage);
            $arr[] = $ar[0];
        }
        return $arr;
    }


    public function retriveTophPerContestPage($url)
    {
        return self::grabData($this->tophSelectors, $url);
    }

    public function retriveTophPerPagination($url)
    {
        return self::grabData($this->tophPaginationSelector, $url);
    }


//     start of grabbing data from hackerrank

    public function hackerRankDataRetrive($url, $filters)
    {
        $crawler = $this->guzzleClient->request('GET', $url);

        $contestLists['NameV'] = $crawler->filter($filters['contestNameSelector'])
            ->each(function (Crawler $node) {
                return $node->text();
            });
        $contestStatusSelector = $filters['contestStatusSelector'];
        $contestLists['status'] = $crawler->filter($contestStatusSelector)
            ->each(function (Crawler $node) use ($crawler, $contestStatusSelector) {
                return $node->text();
            });
        $contestLists['status'] = array_filter($contestLists['status']);
        $contestLists['statusStart'] = $crawler->filter($contestStatusSelector . ' meta:nth-child(1)')
            ->each(function (Crawler $node) {
                if ($node->attr('content') != '') {
                    return date('Y-m-d h:i a', strtotime($node->attr('content')));
                }
//                return $node->attr('content');
            });
        $contestLists['statusEnd'] = $crawler->filter($contestStatusSelector . ' meta:nth-child(2)')
            ->each(function (Crawler $node) {
                if ($node->attr('content') != '') {
                    return date('Y-m-d h:i a', strtotime($node->attr('content')));
                }
            });
//        $contestLists['duration'] = $crawler->filter($contestStatusSelector . ' meta:nth-child(3)')
//            ->each(function (Crawler $node) {
//                return $node->attr('content');
//            });
        $contestLists['detailLink'] = $crawler->filter($filters['contestLinkActiveSelector'])
            ->each(function (Crawler $node) use ($url) {
                return $url . $node->attr('href');
            });

        $contestLists['detailLinkEnded'] = $crawler->filter($filters['contestLinkArchiveSelector'])
            ->each(function (Crawler $node) {
                return $node->text();
            });
        return ($contestLists);

    }

    public function hackerRankContestList()
    {
        $url = "https://www.hackerrank.com/contests";
        $output = Self::hackerRankDataRetrive($url, $this->hackerRankSelectors);
        $output['detailLink'] = array_merge_recursive($output['detailLink'], $output['detailLinkEnded']);
        $output['statusStart'] = array_merge_recursive($output['status'], $output['statusStart']);
        $output['statusEnd'] = array_merge_recursive($output['status'], $output['statusEnd']);
        unset($output['status']);
        unset($output['detailLinkEnded']);
        return $output;
    }
}
