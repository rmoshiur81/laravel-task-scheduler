<?php

namespace App\Http\Controllers;

use App\ContestOrganizerContestList;
class TophContestController extends Controller
{

    protected $contestController;

    function __construct()
    {
        $this->contestController = new ContestController();
    }

    protected function arrangedContestForInsertQuery()
    {
        $data = [];
        $tophContestData = $this->contestController->finallyGrabContestsFromToph();
        $sl = 1;
        foreach ($tophContestData['contestNames'] as $key => $tophContestDatum) {
            $data[$sl] = array(
                'contest_name' => trim($tophContestDatum),
                'contest_detail_page_link' => 'https://toph.co/' . trim($tophContestData['links'][$key]),
                'contest_date_time_status' => trim($tophContestData['start_date_time_status'][$key]),
                'contest_organizer_id' => 2,
            );
            $sl++;
        }
        return array_reverse($data);
    }

    public function saveTophData()
    {
        $data = self::arrangedContestForInsertQuery();
        $totalSaveNow =0;
        foreach ($data as $datum) {
            $tophObj = new ContestOrganizerContestList();
            $tophObj->contest_name = $datum['contest_name'];
            $tophObj->start_date = $datum['contest_date_time_status'];
            $tophObj->end_date = $datum['contest_date_time_status'];
            $tophObj->detail_page_url = $datum['contest_detail_page_link'];
            $tophObj->contest_organizer_id = $datum['contest_organizer_id'];

            $count = ContestOrganizerContestList::where(['contest_name' => $tophObj->contest_name],['contest_organizer_id'=>$tophObj->contest_organizer_id])->get()->count();
            if ($count == 0) {
                if($tophObj->save()){
                    $totalSaveNow++;
                }
            }
        }
        echo 'Total Toph Contest Saved now: '.$totalSaveNow."\n";
    }
}
