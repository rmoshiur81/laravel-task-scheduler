<?php

namespace App\Http\Controllers;

use App\ContestOrganizerContestList;
use Illuminate\Http\Request;

class HackerRankController extends Controller
{
    protected $contestController;

    public function __construct()
    {
        $this->contestController = new ContestController();
    }
    protected function arrangedContestForInsertQuery()
    {
        $data = [];
        $hackerRContestData = $this->contestController->hackerRankContestList();
        $sl = 1;
        foreach ($hackerRContestData['NameV'] as $key => $hackerRContestDatum) {
            $data[$sl] = array(
                'contest_name' => trim($hackerRContestDatum),
                'start_date' => trim($hackerRContestData['statusStart'][$key]),
                'end_date' => trim($hackerRContestData['statusEnd'][$key]),
                'detail_page_url' => trim($hackerRContestData['detailLink'][$key]),
                'contest_organizer_id' => 1,
            );
            $sl++;
        }
//        print_r(array_reverse($data));
        return array_reverse($data);
    }

    public function saveHackerRData()
    {
        $data = self::arrangedContestForInsertQuery();
        $totalSaveNow =0;
        foreach ($data as $datum) {
            $COCLObj = new ContestOrganizerContestList();
            $COCLObj->contest_name = $datum['contest_name'];
            $COCLObj->start_date = $datum['start_date'];
            $COCLObj->end_date = $datum['end_date'];
            $COCLObj->detail_page_url = $datum['detail_page_url'];
            $COCLObj->contest_organizer_id = $datum['contest_organizer_id'];

            $count = ContestOrganizerContestList::where(['contest_name' => $COCLObj->contest_name],['contest_organizer_id'=>$COCLObj->contest_organizer_id])->get()->count();
            if ($count == 0) {
                if($COCLObj->save()){
                    $totalSaveNow++;
                }
            }
        }
        echo 'Total Hacker Rank Contest Saved now: '.$totalSaveNow."\n";
    }
}
