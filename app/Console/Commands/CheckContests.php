<?php

namespace App\Console\Commands;

use App\Http\Controllers\HackerRankController;
use Illuminate\Console\Command;
use App\Http\Controllers\TophContestController;
class CheckContests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckContests:addNewContests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will check the new contest from toph.co/contests';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $toph = new TophContestController();
        $toph->saveTophData();
        $COCL = new HackerRankController();
        $COCL->saveHackerRData();

    }
}
